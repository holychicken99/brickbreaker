# AR Ball Breaker


> AR version of classic Ball Breaker game

![Visitors](https://visitor-badge.glitch.me/badge?page_id=holychicken99.visitor-badge)
![license](https://img.shields.io/badge/license-Creative%20Commons%20Zero%20v1.0%20Universal-brightgreen)
![status](https://img.shields.io/badge/status-beta-important)
## Installation with git

### Dependencies ###

* OpenCV headers and binaries `main sauce`
* Premake `Build automation`
* g++ ` GNU Compiler`
* Make  ` Build Environment`

>:warning: Building tested only on U*ix based OS

```bash
cd ballBreaker

premake5 gmake2

make config={debug/release} 

cd bin/{debug/release}

./ballBreaker 

```








  
    
