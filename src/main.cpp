#include <opencv2/opencv.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <stdexcept>
#include <vector>
#include <iostream>

using namespace cv;

int main()
{

    CascadeClassifier fistCascade;
    fistCascade.load("/home/alpha/Documents/projects/ballBreaker/assets/fist2.xml");
    std::string path{"/home/alpha/Documents/projects/ballBreaker/assets/fist_image.jpg"};
    Mat img = imread(path);

    if (fistCascade.empty())
    {
        std::cerr << "invalid XML file read" << '\n';
    }

    std::vector<Rect> fists;
    fistCascade.detectMultiScale(img, fists);

    for (auto & fist : fists)
    {
        rectangle(img,fist.tl(),fist.br(),Scalar(120,255,167),2);
    }

    imshow("image", img);
    waitKey(0);
    return 0;
}
